
<p><h2>前端：Vue，后端：SpringBoot</h2></p>

<p><h1 align="center">A36.口腔诊所信息管理系统</h1></p>


<p align="center">
	<img src="https://img.shields.io/badge/jdk-1.8-orange.svg"/>
    <img src="https://img.shields.io/badge/springBoot-5.x-lightgrey.svg"/>
    <img src="https://img.shields.io/badge/vue-3.x-blue.svg"/>
    <img src="https://img.shields.io/badge/mysql-5.x-yellow.svg"/>
</p>

## 简介


> 本代码仅供学习参考使用，添加微信（Jnx_1234）获取完整项目!
>
>
>访问路径
>
> http://localhost:8080
>
> http://localhost:8081
>
> 账号密码 : admin admin



## 环境

- <b>IntelliJ IDEA 2009.3</b>

- <b>Mysql 5.7.26</b>

- <b>Tomcat 7.0.73</b>

- <b>JDK 1.8</b>




## 缩略图
![](images/1.png)
![](images/2.png)
![](images/3.png)
![](images/4.png)
![](images/5.png)
![](images/6.png)
![](images/7.png)
![](images/8.png)
![](images/9.png)
![](images/10.png)

## License



##### [更多论文: 全目录查看](https://gitee.com/jiananxu/projects)




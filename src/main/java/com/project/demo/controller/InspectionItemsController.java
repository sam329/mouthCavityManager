package com.project.demo.controller;

import com.project.demo.entity.InspectionItems;
import com.project.demo.service.InspectionItemsService;
import com.project.demo.controller.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *检查项目：(InspectionItems)表控制层
 *
 */
@RestController
@RequestMapping("/inspection_items")
public class InspectionItemsController extends BaseController<InspectionItems,InspectionItemsService> {

    /**
     *检查项目对象
     */
    @Autowired
    public InspectionItemsController(InspectionItemsService service) {
        setService(service);
    }

    @PostMapping("/add")
    @Transactional
    public Map<String, Object> add(HttpServletRequest request) throws IOException {
        Map<String,Object> paramMap = service.readBody(request.getReader());
        this.addMap(paramMap);
        return success(1);
    }

}

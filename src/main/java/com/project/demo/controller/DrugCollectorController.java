package com.project.demo.controller;

import com.project.demo.entity.DrugCollector;
import com.project.demo.service.DrugCollectorService;
import com.project.demo.controller.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *取药员：(DrugCollector)表控制层
 *
 */
@RestController
@RequestMapping("/drug_collector")
public class DrugCollectorController extends BaseController<DrugCollector,DrugCollectorService> {

    /**
     *取药员对象
     */
    @Autowired
    public DrugCollectorController(DrugCollectorService service) {
        setService(service);
    }

    @PostMapping("/add")
    @Transactional
    public Map<String, Object> add(HttpServletRequest request) throws IOException {
        Map<String,Object> paramMap = service.readBody(request.getReader());
        Map<String, String> mapjob_number = new HashMap<>();
        mapjob_number.put("job_number",String.valueOf(paramMap.get("job_number")));
        List listjob_number = service.select(mapjob_number, new HashMap<>()).getResultList();
        if (listjob_number.size()>0){
            return error(30000, "字段工号内容不能重复");
        }
        this.addMap(paramMap);
        return success(1);
    }

}

package com.project.demo.controller;

import com.project.demo.entity.Nurse;
import com.project.demo.service.NurseService;
import com.project.demo.controller.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *护士：(Nurse)表控制层
 *
 */
@RestController
@RequestMapping("/nurse")
public class NurseController extends BaseController<Nurse,NurseService> {

    /**
     *护士对象
     */
    @Autowired
    public NurseController(NurseService service) {
        setService(service);
    }

    @PostMapping("/add")
    @Transactional
    public Map<String, Object> add(HttpServletRequest request) throws IOException {
        Map<String,Object> paramMap = service.readBody(request.getReader());
        Map<String, String> mapnurse_number = new HashMap<>();
        mapnurse_number.put("nurse_number",String.valueOf(paramMap.get("nurse_number")));
        List listnurse_number = service.select(mapnurse_number, new HashMap<>()).getResultList();
        if (listnurse_number.size()>0){
            return error(30000, "字段护士编号内容不能重复");
        }
        this.addMap(paramMap);
        return success(1);
    }

}

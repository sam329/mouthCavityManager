package com.project.demo.controller;

import com.project.demo.entity.ChecklistInformation;
import com.project.demo.service.ChecklistInformationService;
import com.project.demo.controller.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *检查单信息：(ChecklistInformation)表控制层
 *
 */
@RestController
@RequestMapping("/checklist_information")
public class ChecklistInformationController extends BaseController<ChecklistInformation,ChecklistInformationService> {

    /**
     *检查单信息对象
     */
    @Autowired
    public ChecklistInformationController(ChecklistInformationService service) {
        setService(service);
    }

    @PostMapping("/add")
    @Transactional
    public Map<String, Object> add(HttpServletRequest request) throws IOException {
        Map<String,Object> paramMap = service.readBody(request.getReader());
        this.addMap(paramMap);
        return success(1);
    }

}

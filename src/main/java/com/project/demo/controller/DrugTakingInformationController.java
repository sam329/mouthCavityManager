package com.project.demo.controller;

import com.project.demo.entity.DrugTakingInformation;
import com.project.demo.service.DrugTakingInformationService;
import com.project.demo.controller.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *取药信息：(DrugTakingInformation)表控制层
 *
 */
@RestController
@RequestMapping("/drug_taking_information")
public class DrugTakingInformationController extends BaseController<DrugTakingInformation,DrugTakingInformationService> {

    /**
     *取药信息对象
     */
    @Autowired
    public DrugTakingInformationController(DrugTakingInformationService service) {
        setService(service);
    }

    @PostMapping("/add")
    @Transactional
    public Map<String, Object> add(HttpServletRequest request) throws IOException {
        Map<String,Object> paramMap = service.readBody(request.getReader());
        this.addMap(paramMap);
        String sql = "SELECT MAX(drug_taking_information_id) AS max FROM "+"drug_taking_information";
        Query select = service.runCountSql(sql);
        Integer max = (Integer) select.getSingleResult();
        sql = "UPDATE `drug_administration` INNER JOIN `drug_taking_information` ON drug_administration.drug_number=drug_taking_information.drug_number SET drug_administration.drug_inventory = drug_administration.drug_inventory - drug_taking_information.quantity_of_medicine_taken WHERE drug_taking_information.drug_taking_information_id="+max;
        select = service.runCountSql(sql);
        select.executeUpdate();
        return success(1);
    }

}

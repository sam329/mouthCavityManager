package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *手术通知：(OperationNotice)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "OperationNotice")
public class OperationNotice implements Serializable {

    //OperationNotice编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "operation_notice_id")
    private Integer operation_notice_id;
    // 医生编号
    @Basic
    private Integer doctor_number;
    // 医生姓名
    @Basic
    private String name_of_doctor;
    // 患者
    @Basic
    private Integer patient;
    // 姓名
    @Basic
    private String full_name;
    // 手术时间
    @Basic
    private Timestamp operation_time;
    // 手术项目
    @Basic
    private String surgical_items;
    // 结算金额
    @Basic
    private String settlement_amount;
    // 结算状态
    @Basic
    private String settlement_status;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *护士：(Nurse)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "Nurse")
public class Nurse implements Serializable {

    //Nurse编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "nurse_id")
    private Integer nurse_id;
    // 护士编号
    @Basic
    private String nurse_number;
    // 护士姓名
    @Basic
    private String nurse_name;
    // 护士性别
    @Basic
    private String nurse_gender;
    // 用户编号
    @Id
    @Column(name = "user_id")
    private Integer userId;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

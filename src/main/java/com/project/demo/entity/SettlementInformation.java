package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *结算信息：(SettlementInformation)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "SettlementInformation")
public class SettlementInformation implements Serializable {

    //SettlementInformation编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "settlement_information_id")
    private Integer settlement_information_id;
    // 患者
    @Basic
    private Integer patient;
    // 姓名
    @Basic
    private String full_name;
    // 结算类型
    @Basic
    private String settlement_type;
    // 结算时间
    @Basic
    private Timestamp settlement_time;
    // 结算金额
    @Basic
    private String settlement_amount;
    // 备注
    @Basic
    private String remarks;
    // 支付状态
    @Basic
    private String pay_state;

    // 支付类型: 微信、支付宝、网银
    @Basic
    private String pay_type;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *手术信息：(SurgicalInformation)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "SurgicalInformation")
public class SurgicalInformation implements Serializable {

    //SurgicalInformation编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "surgical_information_id")
    private Integer surgical_information_id;
    // 医生编号
    @Basic
    private Integer doctor_number;
    // 患者
    @Basic
    private Integer patient;
    // 姓名
    @Basic
    private String full_name;
    // 手术时间
    @Basic
    private Timestamp operation_time;
    // 手术项目
    @Basic
    private String surgical_items;
    // 手术室
    @Basic
    private String operating_room;
    // 手术内容
    @Basic
    private String operation_content;
    // 手术材料
    @Basic
    private String surgical_materials;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

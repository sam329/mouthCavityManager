package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *检查单信息：(ChecklistInformation)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "ChecklistInformation")
public class ChecklistInformation implements Serializable {

    //ChecklistInformation编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "checklist_information_id")
    private Integer checklist_information_id;
    // 医生编号
    @Basic
    private Integer doctor_number;
    // 患者
    @Basic
    private Integer patient;
    // 姓名
    @Basic
    private String full_name;
    // 检查项目
    @Basic
    private String inspection_items;
    // 检查部位
    @Basic
    private String inspection_position;
    // 检查时间
    @Basic
    private Timestamp inspection_time;
    // 检查结果
    @Basic
    private String inspection_results;
    // 检查备注
    @Basic
    private String check_remarks;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

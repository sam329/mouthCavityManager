package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *检查项目：(InspectionItems)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "InspectionItems")
public class InspectionItems implements Serializable {

    //InspectionItems编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inspection_items_id")
    private Integer inspection_items_id;
    // 检查项目
    @Basic
    private String inspection_items;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *医生信息：(DoctorInformation)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "DoctorInformation")
public class DoctorInformation implements Serializable {

    //DoctorInformation编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "doctor_information_id")
    private Integer doctor_information_id;
    // 医生编号
    @Basic
    private Integer doctor_number;
    // 医生姓名
    @Basic
    private String name_of_doctor;
    // 职业
    @Basic
    private String occupation;
    // 擅长领域
    @Basic
    private String areas_of_expertise;
    // 医生图片
    @Basic
    private String doctor_picture;
    // 门诊类型
    @Basic
    private String outpatient_type;
    // 医生介绍
    @Basic
    private String doctor_introduction;
    // 点击数
    @Basic
    private Integer hits;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

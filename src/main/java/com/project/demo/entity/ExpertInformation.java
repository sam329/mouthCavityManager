package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *专家信息：(ExpertInformation)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "ExpertInformation")
public class ExpertInformation implements Serializable {

    //ExpertInformation编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "expert_information_id")
    private Integer expert_information_id;
    // 医生编号
    @Basic
    private Integer doctor_number;
    // 医生姓名
    @Basic
    private String name_of_doctor;
    // 职业
    @Basic
    private String occupation;
    // 擅长领域
    @Basic
    private String areas_of_expertise;
    // 医生图片
    @Basic
    private String doctor_picture;
    // 门诊类型
    @Basic
    private String outpatient_type;
    // 医生介绍
    @Basic
    private String doctor_introduction;
    // 点击数
    @Basic
    private Integer hits;
	// 计时器标题
	@Basic
	private String timer_title;
	
	// 计时开始时间
	@Basic
	private Timestamp timing_start_time;
	
	// 计时结束时间
	@Basic
	private Timestamp timing_end_time;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

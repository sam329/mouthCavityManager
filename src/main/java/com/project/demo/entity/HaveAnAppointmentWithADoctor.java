package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *预约挂号：(HaveAnAppointmentWithADoctor)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "HaveAnAppointmentWithADoctor")
public class HaveAnAppointmentWithADoctor implements Serializable {

    //HaveAnAppointmentWithADoctor编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "have_an_appointment_with_a_doctor_id")
    private Integer have_an_appointment_with_a_doctor_id;
    // 医生编号
    @Basic
    private Integer doctor_number;
    // 医生姓名
    @Basic
    private String name_of_doctor;
    // 门诊类型
    @Basic
    private String outpatient_type;
    // 患者
    @Basic
    private Integer patient;
    // 姓名
    @Basic
    private String full_name;
    // 预约时间
    @Basic
    private Timestamp time_of_appointment;
    // 接诊时间
    @Basic
    private Timestamp reception_time;
    // 预约状态
    @Basic
    private String reservation_status;
    // 结算状态
    @Basic
    private String settlement_status;
    // 备注
    @Basic
    private String remarks;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

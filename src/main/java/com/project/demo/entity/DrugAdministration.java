package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *药品管理：(DrugAdministration)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "DrugAdministration")
public class DrugAdministration implements Serializable {

    //DrugAdministration编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "drug_administration_id")
    private Integer drug_administration_id;
    // 药品编号
    @Basic
    private String drug_number;
    // 药品名称
    @Basic
    private String drug_name;
    // 药品库存
    @Basic
    private Integer drug_inventory;
    // 药品类型
    @Basic
    private String drug_type;
    // 备注
    @Basic
    private String remarks;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

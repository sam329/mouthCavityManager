package com.project.demo.entity;

import java.sql.Date;
import java.sql.Timestamp;
import com.project.demo.entity.base.BaseEntity;
import java.io.Serializable;
import lombok.*;
import javax.persistence.*;


/**
 *取药信息：(DrugTakingInformation)表实体类
 *
 */
@Setter
@Getter
@Entity(name = "DrugTakingInformation")
public class DrugTakingInformation implements Serializable {

    //DrugTakingInformation编号
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "drug_taking_information_id")
    private Integer drug_taking_information_id;
    // 药品编号
    @Basic
    private String drug_number;
    // 药品名称
    @Basic
    private String drug_name;
    // 取药数量
    @Basic
    private Integer quantity_of_medicine_taken;
    // 取药时间
    @Basic
    private Timestamp drug_taking_time;
    // 患者
    @Basic
    private Integer patient;
    // 姓名
    @Basic
    private String full_name;

    // 更新时间
    @Basic
    private Timestamp update_time;

    // 创建时间
    @Basic
    private Timestamp create_time;

}

package com.project.demo.service;

import com.project.demo.entity.OperationNotice;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 手术通知：(OperationNotice)表服务接口
 *
 */
@Service
public class OperationNoticeService extends BaseService<OperationNotice> {

}

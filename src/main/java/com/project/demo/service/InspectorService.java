package com.project.demo.service;

import com.project.demo.entity.Inspector;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 检查员：(Inspector)表服务接口
 *
 */
@Service
public class InspectorService extends BaseService<Inspector> {

}

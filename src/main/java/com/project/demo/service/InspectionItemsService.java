package com.project.demo.service;

import com.project.demo.entity.InspectionItems;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 检查项目：(InspectionItems)表服务接口
 *
 */
@Service
public class InspectionItemsService extends BaseService<InspectionItems> {

}

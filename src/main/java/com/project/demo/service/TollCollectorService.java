package com.project.demo.service;

import com.project.demo.entity.TollCollector;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 收费员：(TollCollector)表服务接口
 *
 */
@Service
public class TollCollectorService extends BaseService<TollCollector> {

}

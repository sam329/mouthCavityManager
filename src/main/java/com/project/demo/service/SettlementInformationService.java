package com.project.demo.service;

import com.project.demo.entity.SettlementInformation;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 结算信息：(SettlementInformation)表服务接口
 *
 */
@Service
public class SettlementInformationService extends BaseService<SettlementInformation> {

}

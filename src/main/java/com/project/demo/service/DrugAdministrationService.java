package com.project.demo.service;

import com.project.demo.entity.DrugAdministration;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 药品管理：(DrugAdministration)表服务接口
 *
 */
@Service
public class DrugAdministrationService extends BaseService<DrugAdministration> {

}

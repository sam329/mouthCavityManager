package com.project.demo.service;

import com.project.demo.entity.DrugTakingInformation;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 取药信息：(DrugTakingInformation)表服务接口
 *
 */
@Service
public class DrugTakingInformationService extends BaseService<DrugTakingInformation> {

}

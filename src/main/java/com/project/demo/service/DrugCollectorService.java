package com.project.demo.service;

import com.project.demo.entity.DrugCollector;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 取药员：(DrugCollector)表服务接口
 *
 */
@Service
public class DrugCollectorService extends BaseService<DrugCollector> {

}

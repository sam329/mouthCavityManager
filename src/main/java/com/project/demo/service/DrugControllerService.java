package com.project.demo.service;

import com.project.demo.entity.DrugController;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 药管员：(DrugController)表服务接口
 *
 */
@Service
public class DrugControllerService extends BaseService<DrugController> {

}

package com.project.demo.service;

import com.project.demo.entity.ChecklistInformation;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 检查单信息：(ChecklistInformation)表服务接口
 *
 */
@Service
public class ChecklistInformationService extends BaseService<ChecklistInformation> {

}

package com.project.demo.service;

import com.project.demo.entity.Nurse;
import com.project.demo.service.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 护士：(Nurse)表服务接口
 *
 */
@Service
public class NurseService extends BaseService<Nurse> {

}
